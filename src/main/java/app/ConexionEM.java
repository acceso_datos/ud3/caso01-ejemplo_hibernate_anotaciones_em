package app;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

public class ConexionEM {
	private static EntityManager em;

	public static EntityManager getEntityManager() {
		if ((em == null) || (!em.isOpen())) {
			EntityManagerFactory emf = Persistence.createEntityManagerFactory("empresahb");
			em = emf.createEntityManager();
		}
		return em;
	}

	public static void closeEntityManager() {
		if (em != null) {
			em.close();
			em = null;
		}
	}
}
