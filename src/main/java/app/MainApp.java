package app;

import jakarta.persistence.EntityManager;
import modelo.Articulo;
import modelo.Cliente;
import modelo.Factura;
import modelo.Grupo;
import modelo.Linfactura;
import modelo.Vendedor;

/*
 * Mismo ejemplo que los anteriores pero usando JPA de forma estricta. 
 * Se generan instancias de EntityManager en lugar de sesiones de trabajo.
 */
public class MainApp {
	static EntityManager em;

	public static void main(String[] args) {
//		@SuppressWarnings("unused")
		java.util.logging.Logger.getLogger("org.hibernate").setLevel(java.util.logging.Level.SEVERE);

		System.out.println("APP - EMPRESA CON HIBERNATE");
		System.out.println("---------------------------");

		try {
			em = ConexionEM.getEntityManager();
			if (em != null) {
				Articulo a = em.find(Articulo.class, 1);
				System.out.println(a);
				Cliente c = em.find(Cliente.class, 1);
				System.out.println(c);
				Grupo g = em.find(Grupo.class, 1);
				System.out.println(g);
				Vendedor v = em.find(Vendedor.class, 1);
				System.out.println(v);
				Factura f = em.find(Factura.class, 1);
				System.out.println(f);
				for (Linfactura lf : f.getLinfacturas()) {
					System.out.println("\t" + lf);					
				}				

				Grupo g2 = em.find(Grupo.class, 2);
				Articulo artNuevo = new Articulo(g2, "artnuevo", 12f, "cod2", 10);
				em.getTransaction().begin();
				em.persist(artNuevo);
				em.getTransaction().commit();
				System.out.println("Insertado --> " + artNuevo);			
				
				ConexionEM.closeEntityManager();
			}

		} catch (Exception e) {
			System.err.println("Error en alguna operación del programa..." + e.getMessage());
		}

		System.out.println("-------------------------------");
		System.out.println("FIN APP - EMPRESA CON HIBERNATE");

	}

}
